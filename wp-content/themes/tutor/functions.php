<?php
// Exit if accessed directly. 
defined( 'ABSPATH' ) || exit; 
 
if ( ! function_exists( 'understrap_scripts' ) ) { 
 function understrap_scripts() { 
   
  // Get the theme data. 
  $the_theme     = wp_get_theme(); 
  $theme_version = $the_theme->get( 'Version' ); 
 
  $css_version = $theme_version . '.' . filemtime( get_template_directory() . '/libs/bootstrap/bootstrap.min.css' ); 
  wp_enqueue_style( 'bootstrap-styles', get_template_directory_uri() . '/libs/bootstrap/bootstrap.min.css', array(), $css_version ); 
 
   $css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/style.css' ); 
  wp_enqueue_style( 'desktop-styles', get_template_directory_uri() . '/css/style.css', array(), $css_version );
  
     $css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/mobile.css' ); 
  wp_enqueue_style( 'mobile-styles', get_template_directory_uri() . '/css/mobile.css', array(), $css_version );
  
  $js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/jquery.min.js' ); 
  wp_enqueue_script( 'jquery-scripts', get_template_directory_uri() . '/js/jquery.min.js', array(), $js_version, true ); 
  
    $js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/tilt.jquery.min.js' ); 
  wp_enqueue_script( 'tilt-scripts', get_template_directory_uri() . '/js/tilt.jquery.min.js', array(), $js_version, true ); 
  
    $js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/common.js' ); 
  wp_enqueue_script( 'common-scripts', get_template_directory_uri() . '/js/common.js', array(), $js_version, true ); 

 } 
} // End of if function_exists( 'understrap_scripts' ). 
 
add_action( 'wp_enqueue_scripts', 'understrap_scripts' );