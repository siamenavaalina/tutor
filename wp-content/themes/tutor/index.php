<?php get_header(); ?>

  <div id="home" class="wrap">
    <div class="text__block">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12">
            <div class="background_text">
              <p class="headline">PRO English<br><span>with Anastasiya</span></p>
            </div>
          </div>
        </div>

        <div class="col-12"><div class="headline__text">Время открывать новые возможности</div></div>

      </div><!-- container -->
    </div>
  </div><!-- wrap -->

  <div id="about_us" class="box">
    <div class="block__about_me">
      <img class="photo__about_me" src="<?php echo get_template_directory_uri();  ?>/image/tutor-4.jpg" alt="">
      <div class="about_me">
        <div data-tilt class="headline__about_me ">
          Привет!<br>Давай познакомимся?
        </div>

        <div class="text__about_me">
          Я начну :)<br>
          Меня зовут Настя, и я преподаватель английского.<br> Моя задача – помочь тебе побороть страх изучения английского языка и показать, каким весёлым и захватывающим может быть этот процесс.<br> У меня за плечами красный диплом, подтверждённый уровень C2, британский сертификат TESOL уровня А+, обучение в Швеции, опыт работы в сфере образования 3+ и огромная любовь к своему делу. 
        </div>
      </div>
    </div>
  </div><!-- box -->
  <div id="why_us" class="box">

    <div class="container">

      <div class="row justify-content-between">

        <p class="subtitle subtitle--why_us">Начни сейчас, чтобы увидеть результат уже завтра</p>

      </div><!-- .row -->
<div class="number_block">
      <div class="row justify-content-between">
        <div class="col-md-4 col-12 ">

          <div class="box_item">
            <div class="box_item_inner">
              <p data-tilt class="number first_color">01</p>
              <p class="item">Персональный <br>подход</p>
            </div><!-- box_item_inner -->
            <p class="text__item">Мой главный принцип – учить с любовью. Именно поэтому все материалы занятий подбираются и разрабатываются индивидуально в соответствии с потребностями, интересами и уровнем знаний.</p>
          </div><!-- box_item -->

        </div><!-- col-md-4 col-12 -->
        <div class="col-md-4 col-12">

          <div class="box_item">
            <div class="box_item_inner">
              <p  data-tilt class="number second_color">02</p>
              <p class="item">Квалифицированный<br>преподаватель</p>
            </div><!-- box_item_inner -->
            <p class="text__item">Как квалифицированный специалист, я постоянно работаю над совершенствованием своих профессиональных навыков, чтобы создавать контент лучшего качества. </p>
          </div><!-- .box_item -->

        </div><!-- .col -->
        <div class="col-md-4 col-12">

          <div class="box_item">
            <div class="box_item_inner">
              <p data-tilt class="number third_color">03</p>
              <p class="item">Онлайн занятия</p>

            </div><!-- box_item_inner -->
            <p class="text__item">Если ты всё ещё не доверяешь онлайн обучению – пришло время попробовать. Ты сможешь заниматься когда и где угодно из любой точки мира.</p>
          </div><!-- .box_item -->

        </div><!-- .col -->
      </div><!-- row -->
      </div><!-- number_block -->
    </div><!-- container -->

  </div><!-- box -->
  <div id="courses" class="box box--subtitle">
    <div class="container">
      <div class="courses">
        <p class="subtitle">Доступные программы</p>
        <div class="courses_row">
          <div class="row justify-content-center">

            <div class="col-12 col-md-6 col-text-center   col-lg-4 course_column">
              <div class="column_block">
                <p class="courses__headline">General English Course<br>(Elementary A1-A2)</p>
                <img class="courses__image" src="<?php echo get_template_directory_uri();  ?>/image/elementary.jpg">
                <div class="courses_text_block">
                  <p class="text">В основе роста в любой сфере лежит простота. Пройдя этот курс, ты сможешь понимать и использовать простые выражения и базовые грамматические конструкции, направленные на удовлетворение повседневных потребностей.</p>
                  <p class="price_course">10$/час</p>
                </div><!-- courses_text_block -->
              </div><!-- column_block -->
            </div><!-- col-4 -->
            <div class="col-12 col-md-6 col-text-center   col-lg-4 course_column">
              <div class="column_block">
                <p class="courses__headline">General English Course<br>
                  (Pre-Intermediate A2-B1)
                </p>
                <img class="courses__image"src="<?php echo get_template_directory_uri();  ?>/image/preintermediate.jpg">
                <div class="courses_text_block">
                  <p class="text">
                  Если ты владеешь базовым знанием языка, но хочешь двигаться дальше, то этот курс – то, что нужно. После его прохождения ты сможешь строить более сложные и развёрнутые предложения, расширишь список тем для обсуждения,  будешь чётче выражать эмоции, описывать события, надежды и мечты.</p>
                  <p class="price_course">10$/час</p>
                </div><!-- courses_text_block -->
              </div><!-- column_block -->
            </div><!-- col-4 -->
            <div class="col-md-6 col-text-center  col-lg-4 course_column">
              <div class="column_block">
                <p class="courses__headline">General English Course<br>(Intermediate B1-B2)</p>
                <img class="courses__image"  src="<?php echo get_template_directory_uri();  ?>/image/intermediate.jpg">
                <div class="courses_text_block">  
                  <p class="text">
                  Надоели простота и языковое однообразие? Пришла пора освоить новый уровень. После прохождения курса ты сможешь понимать идеи сложных текстов и общаться на абстрактные темы. Улучшишь беглость речи и сможешь спонтанно выражать мысли. Расширишь словарный запас и освоишь все сложные и «страшные» грамматические конструкции.</p>
                  <p class="price_course">12$/час</p>
                </div><!-- courses_text_block -->

              </div><!-- column_block -->
            </div><!-- col-4 -->


            <div class="col-md-6 col-text-center  col-lg-4 course_column">
              <div class="column_block">
                <p class="courses__headline">Business Communication Course<br>(Pre-Intermediate A2-B1)</p>
                <img class="courses__image"  src="<?php echo get_template_directory_uri();  ?>/image/business_preintermediate.jpg">
                <div class="courses_text_block">
                  <p class="text">Задумываешься над изучением бизнес английского? Этот курс поможет освоить базовую лексику для уверенной коммуникации в корпоративной среде. И вопрос о месте работы больше не будет заводить диалог в тупик.</p>
                  <p class="price_course">10$/час</p>
                </div><!-- courses_text_block -->
              </div><!-- column_block -->
            </div><!-- col-4 -->
            <div class="col-md-6 col-text-center  col-lg-4 course_column">
              <div class="column_block">
                <p class="courses__headline">Business Communication Course<br>
                  (Intermediate B1-B2)
                </p>
                <img class="courses__image" src="<?php echo get_template_directory_uri();  ?>/image/business_intermediate.jpg">
                <div class="courses_text_block">
                  <p class="text">
                  В рамках курса этого уровня мы поговорим на темы развития карьеры, проведения деловых встреч и работы в команде. А расширенный словарный запас уверенного Intermediate даст тебе возможность выражать мысли быстро и развёрнуто.</p>
                  <p class="price_course">12$/час</p>
                </div><!-- courses_text_block -->
              </div><!-- column_block -->
            </div><!-- col-4 -->
            <div class="col-md-6 col-text-center  col-lg-4 course_column">
              <div class="column_block">
                <p class="courses__headline">Business Communication Course<br>(Upper-Intermediate B2-C1)</p>
                <img class="courses__image"  src="<?php echo get_template_directory_uri();  ?>/image/business_upperintermediate.jpg">
                <div class="courses_text_block">
                  <p class="text">
                  Улучши навыки и почувствуй себя профи в сфере бизнес коммуникации. Выучи особенности small talk, культурные аспекты и посмотри, как устроен американский бизнес на языке носителя.</p>
                  <p class="price_course">12$/час</p>
                </div><!-- courses_text_block -->
              </div><!-- column_block -->
            </div><!-- col-4 -->
          </div><!-- row -->
        </div><!-- courses_row -->
      </div><!-- courses -->
    </div><!-- container -->
  </div><!-- box -->

<?php get_footer(); ?>

