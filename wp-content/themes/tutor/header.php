
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>"> 
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no,user-scalable=0">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Didact+Gothic&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Fjalla+One&family=Roboto+Slab:wght@300;500;700;800&display=swap" rel="stylesheet">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
 <header class="header">
  <nav class="navbar navbar-expand-lg navbar-light bg-light ">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Pro English</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse " id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto mb-2 mb-lg-0 ">

          <li class="nav-item">
            <img class="nav_image nav_icons" src="<?php echo get_template_directory_uri(); ?>/image/home.svg">
            <a  class="nav-link header__title" href="#home">Главная</a>
          </li>
          <li class="nav-item">
            <img  class="nav_image nav_icons"src="<?php echo get_template_directory_uri(); ?>/image/resume.svg">
            <a class="nav-link header__title" href="#about_us">Обо мне</a>
          </li>
          <li class="nav-item">
            <img  class="nav_image nav_icons"src="<?php echo get_template_directory_uri(); ?>/image/rating.svg">
            <a class="nav-link header__title" href="#why_us">Почему я?</a>
          </li>
          <li class="nav-item">
            <img  class="nav_image nav_icons"src="<?php echo get_template_directory_uri(); ?>/image/teamwork.svg">
            <a class="nav-link header__title" href="#courses">Курсы</a>
          </li>
          <li class="nav-item">
            <img  class="nav_image nav_icons"src="<?php echo get_template_directory_uri(); ?>/image/contacts.svg">
            <a class="nav-link header__title" href="#contacts">Контакты</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

</header>