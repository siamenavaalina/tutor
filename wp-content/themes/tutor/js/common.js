let menu = $('.navbar');
let burger = menu.find('.navbar-toggler');
menu.find('.nav-item a').on('click', function(){
    if(!burger.hasClass('collapsed')){
        burger.trigger('click');
    }
});
